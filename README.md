# Capsule cockpit

Capsule cockpit used by pollux.casa.
Permits to hosted members to manage their capsule

> if you are a pollux.casa member, it is already installed and you can access if on this url : gemini://pollux.casa/member/

## Installation

Choose a folder on your gemini server and put cockpit content in it.

Be sure that your server execute cgi.

You need to install php-cli / php-cgi

Configuration for gemserv

germserv.toml

```
[[server]]
...
cgi = true
## if you want to limit cgi to this folder
cgipath = "/path/to/the/cockpit_folder/"
```

Copy config.sample.php to config.php and adapt it. You can use {user} tag if your server hosts several capsule.

Be careful to give access in read/write to folders and files described in this config.php file.

It is not necessary to install this folder in all your capsule. Configuration should permits you to use an "admin" capsule to manage each user capsule.

