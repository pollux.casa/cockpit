#!/usr/bin/php
<?php
/**
 * This file is part of the Pollux.casa cockpit
 * 
 * Source is available here: https://codeberg.org/pollux.casa/cockpit
 * 
 * @license	GNU/AGPLv3 https://www.gnu.org/licenses/agpl-3.0.html
 * 			see joined LICENSE file
 * 
 * @author	Adële <gemini://adele.work/>
 * 
 **/
 
// Main domain
define('PHPGMI_DOMAIN', 'domain.tld');

// URL of each user capsule
// ("{user}" string will be replace by user account) 
define('PHPGMI_URL_CAPSULE', 'gemini://{user}.domain.tld');
//define('PHPGMI_URL_CAPSULE', 'gemini://domain.tld/~{user}');

// Root directory of each user capsule
// ("{user}" string will be replace by user account) 
define('PHPGMI_DIR_CAPSULE', '/home/{user}/capsule');

// File containaing fingerprints of each user
// ("{user}" string will be replace by user account) 
define('PHPGMI_FILE_FINGERPRINTS', '/home/{user}/fingerprints.txt');

// Working directory of each user, where app data are stored
// ("{user}" string will be replace by user account) 
define('PHPGMI_DIR_WORK', '/home/{user}/.working');

// Working directory, where app shared data are stored
define('PHPGMI_DIR_COMMON', '/var/data/phpgmi_common');

// Directory where session are stored, a session per client certificate
define('PHPGMI_DIR_SESSIONS', '/tmp/phpgmi_sessions');

// Session life time, could be prune after this time (in seconds)
define('PHPGMI_LIFETIME_SESSIONS', 7200);

// Client certificate fingerprint (depends on gemini server software)
define('PHPGMI_FINGERPRINT', isset($_SERVER['TLS_CLIENT_HASH'])?$_SERVER['TLS_CLIENT_HASH']:''); // Gemserv

// Gemini requested URL  (depends on gemini server software)
define('PHPGMI_GEMINI_URL', isset($_SERVER['GEMINI_URL'])?$_SERVER['GEMINI_URL']:'/'); // Gemserv
